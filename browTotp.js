console.log('loading the context script');

let totpListener = browser.runtime.connect({ name:'browTotp' });

let activeTarget = undefined;

function listenForContextClicks() {
  document.addEventListener('click', (e) => {
    if (e.target.nodeName === 'INPUT' && e.target.type === 'text') {
      activeTarget = e;
      totpListener.postMessage({ name: e.target.nodeName, type: e.target.type });
    }
  });
}

listenForContextClicks();

totpListener.onMessage.addListener(function(m) {
  if (activeTarget && activeTarget.target ) {
    console.log(`Injecting: ${m.label}`);
    activeTarget.target.value = m.otp;
    activeTarget = undefined;
  }
});
