const TOTPS_KEY = 'totps';
const ACTIVE_TOTP = 'activeTotp';

let _browser = null;
try {
  _browser = browser;
} catch (e) {
  _browser = chrome;
}

function displayStoredTotpCodes() {
  _browser.storage.local.get(TOTPS_KEY).then((val) => {
    totps = val[TOTPS_KEY];
    if (totps) {
      for (const totp of totps) {
        browser.contextMenus.remove(totp.browTotpId);
        browser.contextMenus.create({
          id: totp.browTotpId,
          parentId: 'brow-totp',
          title: totp.label,
          onclick: (e) => insertTotpCode(e)
        });
      }
    }
  });
}

function insertTotpCode(event) {
  _browser.storage.local.get(TOTPS_KEY).then((val) => {
    totps = val[TOTPS_KEY] || [];
    totp = totps.find(e => e.browTotpId === event.menuItemId);
    const activeTotp = {};
    activeTotp[ACTIVE_TOTP] = totp;
    _browser.storage.local.set(activeTotp);

    const totpFunc = new TOTP();
    const otp = totpFunc.getOTP(totp.secret);

    if (totpListener) {
      totpListener.postMessage({ otp, label: totp.label });
    }
  });
}

function onMessageListener(m) {
  console.log('Received a message', m);
  if (m && m.action === 'reset') {
    _browser.contextMenus.remove('brow-totp');
  } else if (m && m.action === 'refresh') {
    _browser.contextMenus.create({
      id: 'brow-totp',
      title: 'browTOTP',
      contexts: ['editable']
    });
    displayStoredTotpCodes();
  }
}

// Note contextMenus namespace is for other browser support. 
// use menus for FF only.
_browser.contextMenus.create({
  id: 'brow-totp',
  title: 'browTOTP',
  contexts: ['editable']
});

let totpListener;
_browser.runtime.onConnect.addListener((p) => {
  totpListener = p;
});

_browser.runtime.onMessage.addListener(onMessageListener)

displayStoredTotpCodes();
 