const TOTPS_KEY = 'totps';
const ACTIVE_TOTP = 'activeTotp';

const totpImportInput = document.querySelector('#totpFile');
const totpClearButton = document.querySelector('#clear');
const totpDisplay = document.querySelector('[data-id=totpDisplay]');

function clearTotpClick(event) {
  browser.storage.local.remove(TOTPS_KEY)
  browser.storage.local.remove(ACTIVE_TOTP)
  browser.runtime.sendMessage({ action: 'reset' });
  displayStoredTotpCodes();
}

function importTotpClick(event) { 
  if (!(this.files || this.files.length > 0)) {
    console.debug('Nothing to import. Aborting');
    return;
  }

  const file = this.files[0];
  if (file) {
    const reader = new FileReader();
    reader.onload = e => {
      const totps = JSON.parse(e.target.result);

      let id = 0;
      for (const totp of totps) {
        totp.browTotpId = `${id}-browTotp`;
        id++;
      }
      browser.storage.local.remove(TOTPS_KEY).then(() => {
        browser.storage.local.set({ [TOTPS_KEY]: totps }).then((val) => {
          displayStoredTotpCodes();
          browser.runtime.sendMessage({ action: 'refresh' });
        });
      });
    };

    const text = reader.readAsText(file, 'UTF-8');
  }
}

function displayStoredTotpCodes() {
  while (totpDisplay.firstChild) {
    totpDisplay.removeChild(totpDisplay.firstChild);
  }
    
  browser.storage.local.get(TOTPS_KEY).then((val) => {
    const totps = val[TOTPS_KEY];

    if (totps) {
      const dl = document.createElement('dl');
      for (const totp of totps) {
        const dt = document.createElement('dt');
        const dd = document.createElement('dd');

        dt.innerHTML = totp.label;
        dd.innerHTML = `${totp.type} - ${totp.algorithm}`;
        dl.appendChild(dt);
        dl.appendChild(dd);
      }
      totpDisplay.appendChild(dl);
    }
  });
}

displayStoredTotpCodes();

totpImportInput.addEventListener('change', importTotpClick);
totpClearButton.addEventListener('click', clearTotpClick)

